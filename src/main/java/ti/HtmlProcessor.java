// Copyright (C) 2015  Julián Urbano <urbano.julian@gmail.com>
// Distributed under the terms of the MIT License.

package ti;

import com.sun.deploy.util.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

/**
 * A processor to extract terms from HTML documents.
 */
public class HtmlProcessor implements DocumentProcessor {

	private static String DEFAULT_STOPWORDS_PATH = "src/main/resources/stop-words.txt";

    private static String NUMBER_PATTERN = "[0-9,.]]";
    private static String TIME_PATTERN = "[0-9:/-]";

	// P2
	private HashSet<String> stopwords = new HashSet<>();

	public HtmlProcessor() throws IOException {
		this(new File(DEFAULT_STOPWORDS_PATH));
	}

	/**
	 * Creates a new HTML processor.
	 *
	 * @param pathToStopWords the path to the file with stopwords, or {@code null} if stopwords are not filtered.
	 * @throws IOException if an error occurs while reading stopwords.
	 */
	public HtmlProcessor(File pathToStopWords) throws IOException
	{
		// P2
		if (pathToStopWords != null) {
			// Load stopwords
			BufferedReader br = new BufferedReader(new FileReader(pathToStopWords));

			String line;
			while ((line = br.readLine()) != null) {
				stopwords.add(line.trim());
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Tuple<String, String> parse(String html)
	{
		// P2
		// Parse document
		Document doc = Jsoup.parse(html);
        String docText = doc.text();

        String titleText = doc.title();

        String bodyText;
        Element body = doc.body();
        if (body == null) {
            //bodyText = docText.replace(titleText, "");
            bodyText = "";
            titleText = "";

        } else {
            bodyText = body.text();
        }

		return new Tuple<>(titleText, bodyText); // Return title and body separately
	}

	/**
	 * Process the given text (tokenize, normalize, filter stopwords and stemize) and return the list of terms to index.
	 *
	 * @param text the text to process.
	 * @return the list of index terms.
	 */
	public ArrayList<String> processText(String text)
	{
		ArrayList<String> terms = new ArrayList<>();

		// P2
		// Tokenizing, normalizing, stopwords, stemming, etc. 
        ArrayList<String> tokens = this.tokenize(text);
        for(String token : tokens) {
            String normToken = this.normalize(token);
            if (!this.isStopWord(normToken) && !normToken.isEmpty()) {
                String stemToken = this.stem(normToken);
                terms.add(stemToken);
            }
        }

		return terms;
	}

	/**
	 * Tokenize the given text.
	 *
	 * @param text the text to tokenize.
	 * @return the list of tokens.
	 */
	protected ArrayList<String> tokenize(String text)
	{
		ArrayList<String> tokens = new ArrayList<>();

        String[] result = text.split("\\s");
        for (String token : result) {
            if (token.matches(NUMBER_PATTERN)) {
                tokens.add(token);

            } else if (token.matches(TIME_PATTERN)) {
                tokens.add(token);
            } else {
                String[] subTokens = token.toLowerCase().replaceAll("[^a-z0-9']", " ").split("\\s+");
                tokens.addAll(Arrays.asList(subTokens));
            }
        }

		return tokens;
	}

	/**
	 * Normalize the given term.
	 *
	 * @param text the term to normalize.
	 * @return the normalized term.
	 */
	protected String normalize(String text)
	{
		return text;
	}

	/**
	 * Checks whether the given term is a stopword.
	 *
	 * @param term the term to check.
	 * @return {@code true} if the term is a stopword and {@code false} otherwise.
	 */
	protected boolean isStopWord(String term)
	{
		return stopwords.contains(term);
	}

	/**
	 * Stem the given term.
	 *
	 * @param term the term to stem.
	 * @return the stem of the term.
	 */
	protected String stem(String term)
	{
	    Stemmer stemmer = new Stemmer();
	    stemmer.add(term.toCharArray(), term.length());
	    stemmer.stem();

		return stemmer.toString();
	}
}
