// Copyright (C) 2015  Julián Urbano <urbano.julian@gmail.com>
// Distributed under the terms of the MIT License.

package ti;

import java.util.*;

/**
 * Implements retrieval in a vector space with the cosine similarity function and a TFxIDF weight formulation.
 */
public class Cosine implements RetrievalModel
{
	public Cosine()
	{
		// empty
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ArrayList<Tuple<Integer, Double>> runQuery(String queryText, Index index, DocumentProcessor docProcessor)
	{
		// P1
		// Extract the terms from the query
		ArrayList<String> terms = docProcessor.processText(queryText);

		// Calculate the query vector
		ArrayList<Tuple<Integer, Double>> queryVector = this.computeVector(terms, index);

		// Calculate the document similarity
		ArrayList<Tuple<Integer, Double>> results = this.computeScores(queryVector, index);

		return results; // return results
	}

	/**
	 * Returns the list of documents in the specified index sorted by similarity with the specified query vector.
	 *
	 * @param queryVector the vector with query term weights.
	 * @param index       the index to search in.
	 * @return a list of {@link Tuple}s where the first item is the {@code docID} and the second one the similarity score.
	 */
	protected ArrayList<Tuple<Integer, Double>> computeScores(ArrayList<Tuple<Integer, Double>> queryVector, Index index)
	{

		double queryNorm = 0.0;
		for (Tuple<Integer, Double> term : queryVector) {
			queryNorm += term.item2*term.item2;
		}
		queryNorm = Math.sqrt(queryNorm);

		// P1
		HashMap<Integer, Tuple<Integer, Double>> docMap = new HashMap<>();
		for (Tuple<Integer, Double> term : queryVector) {

			ArrayList<Tuple<Integer, Double>> docIDs = index.invertedIndex.get(term.item1);
			for (Tuple<Integer, Double> docTuple : docIDs) {

				Double docNorm = index.documents.get(docTuple.item1).item2;
				Tuple<Integer, Double> docScore = docMap.getOrDefault(docTuple.item1, new Tuple<>(docTuple.item1, 0.0));
				docScore.item2 += (docTuple.item2/docNorm) * (term.item2/queryNorm);
				docMap.put(docScore.item1, docScore);
			}
		}

		ArrayList<Tuple<Integer, Double>> results = new ArrayList<>(docMap.values());

		// Sort documents by similarity and return the ranking
		Collections.sort(results, new Comparator<Tuple<Integer, Double>>()
		{
			@Override
			public int compare(Tuple<Integer, Double> o1, Tuple<Integer, Double> o2)
			{
				return o2.item2.compareTo(o1.item2);
			}
		});
		return results;
	}

	/**
	 * Compute the vector of weights for the specified list of terms.
	 *
	 * @param terms the list of terms.
	 * @param index the index
	 * @return a list of {@code Tuple}s with the {@code termID} as first item and the weight as second one.
	 */
	protected ArrayList<Tuple<Integer, Double>> computeVector(ArrayList<String> terms, Index index)
	{
		ArrayList<Tuple<Integer, Double>> vector = new ArrayList<>();

		HashMap<String, Integer> frequencies = new HashMap<>();
		for (String term : terms) {
			Integer frequency = frequencies.getOrDefault(term, 0);
			frequencies.put(term, frequency + 1);
		}

		// P1
		for (String term : frequencies.keySet()) {

			Tuple<Integer, Double> entry = index.vocabulary.get(term);
			if (entry != null) {
				Double weight = (1 + Math.log(frequencies.get(term))) * entry.item2;
				Tuple tuple = new Tuple<>(entry.item1, weight);
				vector.add(tuple);
			}
		}

		return vector;
	}
}
